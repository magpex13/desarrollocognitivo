package com.example.magno.desarrollocognitivohabilidades;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.TextView;
import android.widget.Toast;

public class CognitivoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cognitivo);

        CanvasView canvasView= (CanvasView) findViewById(R.id.canvasView);

        canvasView.setTextView((TextView) findViewById(R.id.txtCognitivo));

    }

}
