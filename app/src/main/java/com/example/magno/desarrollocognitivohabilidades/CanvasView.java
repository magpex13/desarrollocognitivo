package com.example.magno.desarrollocognitivohabilidades;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Magno on 25/10/2015.
 */
public class CanvasView extends View {

    private Resources resources;
    private Bitmap bmpIcon;
    private Fruta[] vecFruta;
    private TextView textView;
    private Paint paintLinea;
    private float[] vecLineas;
    private int posEscogido;

    private int cantElementos=0;


    public void setTextView(TextView textView) {
        this.textView = textView;
    }

    public CanvasView(Context context,AttributeSet attrs) {

        super(context,attrs);

        resources = context.getResources();

        _init();

    }


    private void _init() {

        vecFruta= new Fruta[5];

        vecFruta[0]=new Fruta(BitmapFactory.decodeResource(resources,R.drawable.manzana),"manzana",0,1,false);
        vecFruta[1]=new Fruta(BitmapFactory.decodeResource(resources,R.drawable.manzana2),"manzana",800,1,true);
        vecFruta[2]=new Fruta(BitmapFactory.decodeResource(resources,R.drawable.manzana3),"manzana",800,300,true);
        vecFruta[3]=new Fruta(BitmapFactory.decodeResource(resources,R.drawable.pina),"piña",0,350,false);
        vecFruta[4]=new Fruta(BitmapFactory.decodeResource(resources,R.drawable.pina2),"piña",800,680,true);


        bmpIcon = BitmapFactory.decodeResource(resources, R.drawable.manzana);

        paintLinea= new Paint();

        paintLinea.setColor(Color.GREEN);

        posEscogido=-1;

       /* Matrix matrix= new Matrix();
        matrix.postScale((float) (200.0 / (float) bmpIcon.getWidth()), (float) (200.0 / (float) bmpIcon.getHeight()));
        bmpIcon=Bitmap.createBitmap(bmpIcon,0,0,bmpIcon.getWidth(),bmpIcon.getHeight(),matrix,true);*/

        //bmpIcon= Bitmap.createScaledBitmap(bmpIcon,100,100,false);

        //bmpIcon =scaleDown(bmpIcon,Math.max(bmpIcon.getWidth(),bmpIcon.getHeight()),true);


    }

    private boolean _insertarLinea(float xStart,float yStart,float xStop,float yStop)
    {
        if(vecLineas==null)
        {
            vecLineas= new float[4];

            vecLineas[0]=xStart;
            vecLineas[1]=yStart;
            vecLineas[2]=xStop;
            vecLineas[3]=yStop;

            cantElementos=4;
            return true;
        }

        float aux[]= new float[cantElementos+4];

        for (int i=0 ; i<vecLineas.length ; i++)
        {
            aux[i]=vecLineas[i];
        }

        aux[cantElementos]=xStart;
        aux[cantElementos+1]=yStart;
        aux[cantElementos+2]=xStop;
        aux[cantElementos+3]=yStop;

        vecLineas=aux;

        cantElementos+=4;

        return true;
    }

    private boolean _lineaValida(int posDestino)
    {
        if(posEscogido==-1)
            return false;

        if(posEscogido==posDestino)
            return false;

        if(!vecFruta[posEscogido].getStrNombre().equals(vecFruta[posDestino].getStrNombre()))
            return false;

        if(vecFruta[posDestino].isesDestino() && vecFruta[posEscogido].isesDestino())
            return false;

        return true;
    }

    private boolean _touchImagen(float xTouch,float yTouch)
    {
        for (int i=0 ; i<vecFruta.length ; i++)
        {
            if(_seleccionarImagen(vecFruta[i].getxFruta(),vecFruta[i].getyFruta(),vecFruta[i].getBmpFruta().getWidth(),vecFruta[i].getBmpFruta().getHeight(),xTouch,yTouch)) {

                if(posEscogido!=-1)
                {
                    if(_lineaValida(i))
                    {
                        //Insertar nueva linea
                        float xStart=vecFruta[posEscogido].getxFruta()+(vecFruta[posEscogido].getBmpFruta().getWidth()/2);
                        float yStart=vecFruta[posEscogido].getyFruta()+(vecFruta[posEscogido].getBmpFruta().getHeight()/2);
                        float xStop=vecFruta[i].getxFruta()+(vecFruta[i].getBmpFruta().getWidth()/2);
                        float yStop=vecFruta[i].getyFruta()+(vecFruta[i].getBmpFruta().getHeight()/2);

                        _insertarLinea(xStart,yStart,xStop,yStop);

                    }

                    posEscogido=-1;
                    return true;
                }
                posEscogido=i;
                //textView.setText(vecFruta[i].getStrNombre());
                return true;
            }
        }
        return false;
    }

    private boolean _seleccionarImagen(float xImg,float yImg,int wImg,int hImg,float xTouch,float yTouch)
    {
        if(xTouch<=xImg+ wImg && xTouch>=xImg && yTouch<=yImg+hImg && yTouch>=yImg) {
            return true;
        }
        return false;
    }

    @Override
    public void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        //canvas.save();

        //canvas.scale(canvas.getWidth() / DWIDTH, canvas.getHeight() / DHEIGHT);
        //canvas.scale((float)0.5, (float)0.5);




        //canvas.drawLine(vecFruta[0].getxFruta()+vecFruta[0].getBmpFruta().getWidth(),vecFruta[0].getyFruta()+(vecFruta[0].getBmpFruta().getHeight()/2),vecFruta[2].getxFruta(),vecFruta[2].getyFruta()+(vecFruta[2].getBmpFruta().getHeight()/2),paintLinea);




        //canvas.restore();


        canvas.drawColor(0xFF000000);

        for (int i=0 ; i<vecFruta.length ; i++)
        {
            canvas.drawBitmap(vecFruta[i].getBmpFruta(),vecFruta[i].getxFruta(),vecFruta[i].getyFruta(),null);
        }

        if(vecLineas!=null)
            canvas.drawLines(vecLineas,paintLinea);

        invalidate();

    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.


        //textView.setText(String.valueOf(e.getX() + " + " + String.valueOf(e.getY())));


        if(e.getAction()==e.ACTION_UP)
        _touchImagen(e.getX(), e.getY());


        return true;
    }
}
